This project's goal is to create an application (to be used with the Arduino platform) serving as wild animal movement monitoring tool. The Arduino will be equiped with a BT adapter and an IR movement detector and will count and store the times of movement detections. These values will then be sent via the bluetooth interface to the user's device (when it comes into range) which will report them to the user. The main idea is to provide the cheapest and most comfortable possible way of monitoring wild animal movements, counts, feeding times, etc..
One possible way of advancing the project would be to use multiple IR movement detectors in order to determine the directional movement of the animals. This could in theory work for lone animals however it has to be determined wheterher it provides any benefits to the intended use case of determining the usual/most frequent animal (or animal herd) feeding/arrival times at the location.

The Android application part of this project uses the following third party libraries released under the Apache License 2.0 (available from: https://www.apache.org/licenses/LICENSE-2.0):
�	Apache Commons IO 2.5
�	ButterKnife 8.4.0
�	GNU Trove4J 3.0.1
�	Realm 2.2.1
