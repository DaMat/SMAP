/*
#include <FreeStack.h>
#include <MinimumSerial.h>
#include <SdFat.h>
#include <SdFatConfig.h>
#include <SdFatUtil.h>
#include <SystemInclude.h>
SdFat SD;
*/

#include <SPI.h>
#include <SD.h>
#include <stdlib.h>
#include <SoftwareSerial.h>


char nCur[] = "CURRENT";   
int inPin = 5;      // select the input pin for the PIR
int ledPin = 2;     // select the pin for the LED
int oldValue = 0;   // variable to store the last value
int count = 0;
long readTime = 0;  //time of last sending of values

SoftwareSerial Bluetooth = SoftwareSerial(6, 7);
File f;

void setup() {
  Serial.begin(9600);
  Serial.println("set-call");
  Bluetooth.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(inPin,INPUT);
  if (!SD.begin(4)) {
    Serial.println("set-SDerr");
    return;
  }
  File from = SD.open(nCur, FILE_READ);
  uint64_t zTime = readZeroTime();
  if (zTime > 0 && from && from.size() > 20){ //if there is some old time present and there are more than 5 values saved in the CURRENT file (still present from the last boot)
         Serial.println("set-copy_call");
         char toName[9];
         itoa2(zTime, toName);
         File to = SD.open(toName, FILE_WRITE);
         if (!to){
            Serial.println("set-copy_error");
         }
         write64ToFile(to, zTime);
         to.flush();
         while (from.available() > 0){
           char buf[50];
           int n = from.read(buf, 50);
           to.write(buf, n);
           to.flush();
         }
         writeToFile(to, (long) 0); //trailing 4 zero bytes indicating EOF
         File pending = SD.open("PENDING", FILE_WRITE); //append the timestamp to the file containing pending timestamps
         write64ToFile(pending, zTime);
         pending.flush();
         pending.close();
         from.close();
         to.flush();
         to.close();
         Serial.println("set-copy_fin");
  }  
  SD.remove("ZEROTIME");
  openFile();
  Serial.println("set-fin");
  delay(500);
}

  void loop() {
    int sensorValue = digitalRead(inPin);
    if (oldValue == LOW && sensorValue == HIGH){
      digitalWrite(ledPin, HIGH);
      saveTimestamp();
      Serial.println(millis());
    } else if (sensorValue == LOW){
      digitalWrite(ledPin, LOW);
    }
    oldValue = sensorValue;
  
  if (Bluetooth.available()){
    char in = Bluetooth.read();
    if (in == 1 || in == 2){
      Serial.println("bt-read");
      uint8_t realTimeBytes[8];
      Bluetooth.readBytes(realTimeBytes, 8);
      uint64_t realTime = btoint64(realTimeBytes);
      if (in == 2 && readZeroTime() > 0){ //set time request and no time is saved
          saveZeroTime(realTime - (uint64_t) millis());
      }
      if (in == 1){ //read data request
          long futureReadTime = millis();
          saveZeroTime(realTime);
          sendData(realTime - millis() + readTime);
          readTime = futureReadTime;
      }
    }
  }
  delay(500);
}


void sendData(uint64_t zeroTime){
   Serial.println("send-call");
   File pendingF = SD.open("PENDING", FILE_READ);
   int pendingCount = 0;
   if (pendingF){
        pendingCount = pendingF.size()/8;
   }
   writeToBT(pendingCount + 1); //first 4 bytes are the number of value list expected (from pending file + 1 current)
   for (int i = 0; i < pendingF.size()/8; i++){ //iterate over all pending files
       uint8_t nameBytes[8];
       pendingF.readBytes(nameBytes, 8);
       char fileName[9];
       itoa2(btoint64(nameBytes), fileName);
       File file = SD.open(fileName, FILE_READ);
       if (file){
             while (file.available() > 0){
               char buf[50];
               int n = file.read(buf, 50);
               Bluetooth.write(buf,n);
             }
       } else {
           //write64ToBT(4611686018427387904LL); //send 8 random bytes for correct parsing on android
           writeToBT((long) 0); //write 0 only if there was an issue with the file (files contain the trailing zero)
       }
       
   }
   SD.remove("PENDING"); // delete the pending file, assuming all the data was sent and received
   Serial.println("send-pending removed");
   char toName[9];
   itoa2(zeroTime, toName);
   f.seek(0);
   File to = SD.open(toName, FILE_WRITE); 
   write64ToBT(zeroTime);
   write64ToFile(to, zeroTime);
   to.flush();
   while (f.available() > 0){
     char buf[50];
     int n = f.read(buf, 50);
     Bluetooth.write(buf,n);
     to.write(buf, n);
     to.flush();
   }
   writeToBT((long) 0); //trailing 4 zero bytes indicating EOF
   writeToFile(to, (long) 0);
   f.close();
   to.flush();
   to.close();
   openFile(); //reopen new current file
   Serial.println("send-fin");
}

void writeToSerial(long x){
  byte b[4];
  integerToBytes(x, b);
  Serial.write(b, 4);
}

void writeToBT(long x){
  byte b[4];
  integerToBytes(x, b);
  Bluetooth.write(b, 4);
}

void write64ToBT(uint64_t x){
  byte b[8];
  i64tob(x, b);
  Bluetooth.write(b, 8);
}

void writeToFile(File file, long x){
  byte b[4];
  integerToBytes(x, b);
  file.write(b, 4);
  file.flush();
}

void write64ToFile(File file, uint64_t x){
    byte b[8];
    i64tob(x, b);
    file.write(b, 8);
    file.flush();
}

/*
 * Saves the current millis() - readTime to the "f" file containg current values
 */
void saveTimestamp(){
  byte b[4];
  long x = millis() - readTime;
  integerToBytes(x, b);
  f.write(b, 4);
  f.flush();
}

/*
 * Saves the zero time to the "ZEROTIME" file deleting existing one
 */
void saveZeroTime(uint64_t zeroTime){
  if(SD.remove("ZEROTIME")){
    Serial.println("zt-old_rem");  
  } else {
    Serial.println("zt-old_NOT_rem");  
  }
  File ztFile = SD.open("ZEROTIME", FILE_WRITE);
  if (!ztFile){
    Serial.println("zt-save_err");   
    return; 
  }
  write64ToFile(ztFile, zeroTime);
  ztFile.close();
  Serial.println("zt-saved");
}

/*
 * Returns realworld zeroTime saved in the "ZEROTIME" file or 0 if the file could not be read (or is non existing)
 */
uint64_t readZeroTime(){
  uint64_t zTime = 0LL;
  File ztFile = SD.open("ZEROTIME");
  if (ztFile) {
    uint8_t ztBytes[8];
    ztFile.readBytes(ztBytes, 8);
    zTime = btoint64(ztBytes);
    ztFile.close();
    Serial.println("zt-read_fin");
  } else {
    Serial.println("zt-read_err");  
  }
  return zTime;
}

/*
 * Converts 8bytes big endian to u_int64.
 */
uint64_t btoint64(byte b[8]){
  uint64_t val = (uint64_t) b[0] << 56 | (uint64_t) (b[1] & 0xFF) << 48 | (uint64_t) (b[2] & 0xFF) << 40 | (uint64_t) (b[3] & 0xFF) << 32 | (uint64_t)(b[4] & 0xFF) << 24 | (uint64_t)(b[5] & 0xFF) << 16 | (uint64_t)(b[6] & 0xFF) << 8 | (uint64_t)(b[7] & 0xFF);
  return val;
}

/*
 * Converts long (4bytes) to byte array Big endian
 */
void integerToBytes(long val, byte b[4]) {
  b[0] = (byte )((val >> 24) & 0xff);
  b[1] = (byte )((val >> 16) & 0xff);
  b[2] = (byte )((val >> 8) & 0xff);
  b[3] = (byte )(val & 0xff);
}

/*
 * Converts uint64 (8bytes) to byte array Big endian
 */
void i64tob(uint64_t val, byte b[8]){
  b[0] = (byte)((val >> 56) & 0xff);
  b[1] = (byte)((val >> 48) & 0xff);
  b[2] = (byte)((val >> 40) & 0xff);
  b[3] = (byte)((val >> 32) & 0xff);
  b[4] = (byte)((val >> 24) & 0xff);
  b[5] = (byte)((val >> 16) & 0xff);
  b[6] = (byte)((val >> 8) & 0xff);
  b[7] = (byte)(val & 0xff);
}

/*
 * Opens new current values file (variable "f") deleting the old one
 */
void openFile(){
  SD.remove(nCur);
  f = SD.open(nCur, FILE_WRITE);
  if (!f){
    Serial.println("cur-open_err");  
  } else {
    Serial.println("cur-fin");  
  }
}

/*
 * Converts the 8 byte int value to char array in base36
 */
void itoa2(uint64_t value, char* str){
  char* p = str;
  while (value){
    uint64_t digit = value % 36;
    *p++ = (digit < 10) ? ('0' + digit) : ('A' + digit - 10);
    value /= 36;
  }
  *p-- = '\0';

  // reverse the string
  char* q = str;
  while (p > q){
    char c = *p;
    *p-- = *q;
    *q++ = c;
  }
}
