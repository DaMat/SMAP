package cz.damat.venisontracker.helpers;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import cz.damat.venisontracker.model.Device;
import cz.damat.venisontracker.model.Timestamp;
import gnu.trove.map.TLongIntMap;
import gnu.trove.map.hash.THashMap;
import gnu.trove.map.hash.TLongIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TLongIntProcedure;
import gnu.trove.procedure.TLongProcedure;

/**
 * Created by Matej Danicek on 1.1.2017.
 */
public class Histogram {
    private static final long MS_MINUTE = 1000 * 60;
    private static final long MS_HOUR = MS_MINUTE * 60;
    private static final long MS_DAY = MS_HOUR * 24;
    private static final long MS_WEEK = MS_DAY * 7;

    TLongIntMap mapMinutes = new TLongIntHashMap();
    TLongIntMap mapHours = new TLongIntHashMap();
    TLongIntMap mapDays = new TLongIntHashMap();
    TLongIntMap mapWeeks = new TLongIntHashMap();
    boolean calculated = false;
    Device device;

    public Histogram(Device device) {
        this.device = device;
    }

    public void calculate() {
        long tss[] = device.getTimestampsArray();
        GregorianCalendar zeroCal = new GregorianCalendar();
        zeroCal.setTimeInMillis(tss[0]);
        zeroCal.set(Calendar.MILLISECOND, 0);
        zeroCal.set(Calendar.SECOND, 0);
        long zeroMinute = zeroCal.getTime().getTime();
        zeroCal.set(Calendar.MINUTE, 0);
        long zeroHour = zeroCal.getTime().getTime();
        zeroCal.set(Calendar.HOUR, 0);
        long zeroDay = zeroCal.getTime().getTime();
        zeroCal.set(Calendar.DAY_OF_WEEK, 0);
        long zeroWeek = zeroCal.getTime().getTime();

        for (long ts : tss) {
            mapMinutes.adjustOrPutValue(((ts - zeroMinute) / MS_MINUTE) * MS_MINUTE + zeroMinute, 1, 1);
            mapHours.adjustOrPutValue(((ts - zeroHour) / MS_HOUR) * MS_HOUR + zeroHour, 1, 1);
            mapDays.adjustOrPutValue(((ts - zeroDay) / MS_DAY) * MS_DAY + zeroDay, 1, 1);
            mapWeeks.adjustOrPutValue(((ts - zeroWeek) / MS_WEEK) * MS_WEEK + zeroWeek, 1, 1);
        }
        calculated = true;
    }

    /**
     * @return value of {@link #mapMinutes}
     **/
    public TLongIntMap getMapMinutes() {
        if (!calculated) calculate();
        return mapMinutes;
    }

    /**
     * @return value of {@link #mapHours}
     **/
    public TLongIntMap getMapHours() {
        if (!calculated) calculate();
        return mapHours;
    }

    /**
     * @return value of {@link #mapDays}
     **/
    public TLongIntMap getMapDays() {
        if (!calculated) calculate();
        return mapDays;
    }

    /**
     * @return value of {@link #mapWeeks}
     **/
    public TLongIntMap getMapWeeks() {
        if (!calculated) calculate();
        return mapWeeks;
    }
}
