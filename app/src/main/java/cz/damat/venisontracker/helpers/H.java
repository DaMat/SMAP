package cz.damat.venisontracker.helpers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import gnu.trove.list.array.TByteArrayList;

/**
 * Class with static helper methods for parsing, formatting etc.
 */
public class H {

    public static BluetoothDevice getDeviceByMAC(String mac) {
        for (BluetoothDevice device : BluetoothAdapter.getDefaultAdapter().getBondedDevices()) {
            if (device.getAddress().compareToIgnoreCase(mac) == 0) {
                return device;
            }
        }
        return null;
    }

    /**
     * Parse big endian bytes to java int
     *
     * @param bytes either 2 or 4 bytes with least significant byte last
     * @return parsed int or 0 if @param bytes has wrong length
     */
    public static int toInt(byte[] bytes) {
        if (bytes.length != 2 && bytes.length != 4) return 0;
        int i = bytes[3];
        i += bytes[2] << 8;
        if (bytes.length == 4) {
            i += bytes[1] << 16;
            i += bytes[0] << 24;
        }
        return i;
    }

    public static int parseInt(TByteArrayList list, int s) {
        byte[] bytes = new byte[]{list.get(s), list.get(s + 1), list.get(s + 2), list.get(s + 3)};
        int i = bytes[0] << 24 | (bytes[1] & 0xFF) << 16 | (bytes[2] & 0xFF) << 8 | (bytes[3] & 0xFF);
        return i;
    }

    /**
     * Big endian
     * @param list
     * @param s
     * @return
     */
    private static long parseLong(TByteArrayList list, int s) {
        byte[] bytes = new byte[]{list.get(s), list.get(s + 1), list.get(s + 2), list.get(s + 3), list.get(s + 4), list.get(s + 5), list.get(s + 6), list.get(s +7)};
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();//need flip
        long i = buffer.getLong();
        return i;
    }

    /**
     * Parses the specific format input stream to array of real-world utc timestamps
     *
     * @param list
     * @param timestampAndroid android time stamp corresponding with the first number in data
     *                         (arduino time stamp at the time of sending)
     * @return
     * @throws IOException
     */
    public static long[] parseInputStream(TByteArrayList list, long timestampAndroid) throws IOException {
        if (list == null) return new long[]{};
        long zeroTime = parseLong(list, 0); //arduino time when it received the data request = first 8 bytes in data
        long[] values = new long[(list.size() / 4) - 2 - 1]; //minus two ints for timestamp and one int for trailing zero

        int value;
        int previousValue = 0;
        int j = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss.mmm");
        for (int i = 8; i < list.size() - 4; i += 4) {
            value = parseInt(list, i);
            if (value < previousValue) { //the arduino timer has overflown (after 50 days)
                break; //exit the loop (ignore the values since there is no way we can know how many times it overflew
            }
            values[j] = zeroTime + value;
            Log.d("DATE PARSE", sdf.format(new Date(zeroTime + value)));
            previousValue = value;
            j++;
        }
        return values;
    }
}
