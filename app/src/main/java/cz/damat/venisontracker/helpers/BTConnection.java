package cz.damat.venisontracker.helpers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cz.damat.venisontracker.helpers.H;
import gnu.trove.list.array.TByteArrayList;

/**
 * Class for sending the data sending initiation bytes and reading and handling the received data
 */
public class BTConnection {
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static byte REQUEST_DATA = 1;
    private static byte MARKER_MESSAGE_END = 0;

    private Thread comThread;

    public boolean readValues(final BluetoothDevice device, final ReadResultListener listener) {
        final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (device == null || listener == null || adapter == null || !adapter.isEnabled() || comThread != null)
            return false;
        comThread = new Thread(new Runnable() {
            @Override
            public void run() {
                BluetoothSocket socket = null;
                InputStream is = null;
                OutputStream os = null;
                try {
                    adapter.cancelDiscovery();
                    socket = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                    listener.onProgress(ReadResultListener.PROGRESS_CONNECTING);
                    socket.connect();
                    listener.onProgress(ReadResultListener.PROGRESS_SENDING);
                    is = socket.getInputStream();
                    os = socket.getOutputStream();
                    os.write(REQUEST_DATA); //send request identifier

                    long timeStampLong = System.currentTimeMillis();
                    ByteBuffer buffer = ByteBuffer.allocate(8);
                    buffer.putLong(timeStampLong);
                    byte[] timestamp = buffer.array();
                    os.write(timestamp);
                    os.flush();

                    listener.onProgress(ReadResultListener.PROGRESS_RECEIVING);
                    TByteArrayList listCountBytes = new TByteArrayList();
                    for (int j = 0; j < 4; j++) {
                        listCountBytes.add((byte) is.read());
                    }
                    int listCount = H.parseInt(listCountBytes, 0);
                    int a;
                    int sum = 0;
                    int listCountRead = 0;
                    int i = 0;
                    List<TByteArrayList> byteLists = new ArrayList<>(listCount);

                    TByteArrayList byteList = new TByteArrayList();
                    while ((a = is.read()) >= 0) {
                        Log.d("READ: ", String.valueOf(a));
                        byteList.add((byte) a);
                        i++;
                        sum += a;
                        if (i % 4 == 0) { // read 4 bytes
                            if (sum == 0) { //read 4 zero bytes
                                if (byteList.size() > 8) { //dont add empty lists
                                    byteLists.add(byteList);
                                }
                                listCountRead++;
                                if (listCountRead == listCount) break;
                                byteList = new TByteArrayList();
                            } else {
                                sum = 0;
                            }
                        }
                    }

                    is.close();
                    os.close();
                    socket.close();

                    listener.onProgress(ReadResultListener.PROGRESS_PARSING);
                    long timestamps[][] = new long[byteLists.size()][];
                    for (i = 0; i < byteLists.size(); i++) {
                        timestamps[i] = H.parseInputStream(byteLists.get(i), timeStampLong);
                    }
                    listener.onValuesReceived(timestamps);
                } catch (IOException e) {
                    e.printStackTrace();
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException ignored) {
                            ignored.printStackTrace();
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                    if (os != null) {
                        try {
                            os.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                    listener.onError(e);
                }
            }
        });
        comThread.start();
        return true;
    }

    public interface ReadResultListener {
        int PROGRESS_CONNECTING = 0x01;
        int PROGRESS_SENDING = 0x02;
        int PROGRESS_RECEIVING = 0x03;
        int PROGRESS_PARSING = 0x04;

        void onProgress(int progressType);

        void onValuesReceived(long[][] values);

        void onError(Exception e);
    }
}
