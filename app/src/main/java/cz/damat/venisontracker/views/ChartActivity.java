package cz.damat.venisontracker.views;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import cz.damat.venisontracker.R;
import cz.damat.venisontracker.helpers.C;
import cz.damat.venisontracker.helpers.H;
import cz.damat.venisontracker.helpers.Histogram;
import cz.damat.venisontracker.model.Device;
import gnu.trove.map.TLongIntMap;
import gnu.trove.procedure.TLongIntProcedure;

public class ChartActivity extends SuperClassActivity {
    static final int MODE_MINUTES = 0x00;
    static final int MODE_HOURS = 0x01;
    static final int MODE_DAYS = 0x02;
    static final int MODE_WEEKS = 0x03;

    BarChart chart;
    BarDataSet sets[] = new BarDataSet[4];
    float barWidth[] = new float[]{60f, 60 * 60f, 60 * 60 * 24f, 60 * 60 * 24 * 7f};

    Device device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        chart = (BarChart) findViewById(R.id.chart);
        device = Device.getByMAC(getIntent().getExtras().getString("MAC"));
        if (device.getTimestamps().isEmpty()){
            Toast.makeText(this, R.string.no_values, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        setTitle(device.toString());
        showProgressDialog(R.string.loading);
        new Thread(new Runnable() {
            @Override
            public void run() {
                fillDataSets();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            hideProgressDialog();
                            switchToMode(MODE_HOURS);
                        } catch (Exception ignored) {
                        }
                    }
                });
            }
        }).run();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String labels[] = new String[]{
                getString(R.string.minutes),
                getString(R.string.hours),
                getString(R.string.days),
                getString(R.string.weeks),
        };
        for (int i = 0; i <= MODE_WEEKS; i++){
            menu.add(0,i,i, labels[i]).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
            default:
                switchToMode(item.getItemId());
                return true;

        }
    }


    private void switchToMode(int mode) {
        BarData data = new BarData(sets[mode]);
        data.setBarWidth(barWidth[mode]);
        chart.setData(data);
        chart.animateXY(500, 500);
        chart.getAxisLeft().setAxisMinimum(0f);
        chart.getAxisRight().setAxisMinimum(0f);
        chart.setFitBars(true); // make the x-axis fit exactly all bars
        final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy\nHH:mm");
        chart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return sdf.format(new Date((C.TS_OFFSET + (long) value * 1000)));
            }
        });
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.invalidate();
    }

    private void fillDataSets() {
        Histogram h = new Histogram(device);
        TLongIntMap values[] = {h.getMapMinutes(), h.getMapHours(), h.getMapDays(), h.getMapWeeks()};
        for (int i = 0; i <= MODE_WEEKS; i++) {
            final List<BarEntry> entries = new ArrayList<>();
            values[i].forEachEntry(new TLongIntProcedure() {
                @Override
                public boolean execute(long a, int b) {
                    entries.add(new BarEntry((a - C.TS_OFFSET) / 1000, b));
                    return true;
                }
            });
            Collections.sort(entries, new Comparator<BarEntry>() {
                @Override
                public int compare(BarEntry entry1, BarEntry entry2) {
                    return (int) (entry1.getX() - entry2.getX());
                }
            });
            sets[i] = new BarDataSet(entries, null);
        }
    }

}