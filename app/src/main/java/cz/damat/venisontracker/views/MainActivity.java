package cz.damat.venisontracker.views;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import cz.damat.venisontracker.R;
import cz.damat.venisontracker.model.Device;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class MainActivity extends Activity {
    private static String MAC = "98:D3:31:40:2C:0C";

    private ListView listView;
    private ArrayAdapter<Device> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listDevices);
        RealmResults<Device> devices = Device.getAll();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, devices);
        devices.addChangeListener(new RealmChangeListener<RealmResults<Device>>() {
            @Override
            public void onChange(RealmResults<Device> element) {
                adapter.notifyDataSetChanged();
            }
        });
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Device device = adapter.getItem(i);
                Intent intent = new Intent(MainActivity.this, ChartActivity.class);
                intent.putExtra("MAC", device.getMAC());
                startActivity(intent);
            }
        });
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, 0, 0, R.string.device_edit);
        menu.add(0, 1, 1, R.string.device_remove);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Device device = adapter.getItem(info.position);
        if (device == null) return false;
        switch (item.getItemId()) {
            case 0:
                Intent intent = new Intent(MainActivity.this, DeviceActivity.class);
                intent.putExtra("MAC", device.getMAC());
                startActivity(intent);
                return true;
            case 1:
                AlertDialog.Builder b = new AlertDialog.Builder(this);
                b.setMessage(R.string.device_remove_confirm);
                b.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        device.deleteFromRealm();
                        realm.commitTransaction();
                    }
                });
                b.setNegativeButton(R.string.no, null);
                b.show();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.add).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, 1, 1, R.string.connect).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        menu.add(0, 2, 2, R.string.map).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0: //add device
                Intent i = new Intent(this, ScanActivity.class);
                i.putExtra("ADDING", true);
                startActivity(i);
                break;
            case 1: //read values
                Intent j = new Intent(this, ScanActivity.class);
                j.putExtra("ADDING", false);
                startActivity(j);
                break;
            case 2:
                startActivity(new Intent(MainActivity.this, MapActivity.class));
                break;
            default:
                return false;
        }
        return true;
    }
}
