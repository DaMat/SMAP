package cz.damat.venisontracker.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.damat.venisontracker.R;
import cz.damat.venisontracker.model.Device;
import io.realm.Realm;

public class DeviceActivity extends Activity {
    int PLACE_PICKER_REQUEST = 1;

    @BindView(R.id.textName)
    TextView textName;
    @BindView(R.id.textMAC)
    TextView textMAC;
    @BindView(R.id.textLat)
    TextView textLat;
    @BindView(R.id.textLong)
    TextView textLong;
    @BindView(R.id.btnMap)
    Button btnMap;

    Device device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null && getIntent().getExtras().getString("MAC") != null) {
            device = Device.getByMAC(getIntent().getExtras().getString("MAC"));
            textMAC.setText(getIntent().getExtras().getString("MAC"));
        }
        if (device != null) {
            textName.setText(device.getName());
            textMAC.setText(device.getMAC());
            textLat.setText(String.valueOf(device.getLatitude()));
            textLong.setText(String.valueOf(device.getLongitude()));
            setTitle(device.toString());
        } else {
            setTitle(R.string.device_new_title);
        }
        btnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(DeviceActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    Toast.makeText(DeviceActivity.this, R.string.device_map_error, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        askBeforeExit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place != null){
                    textLat.setText(String.valueOf(place.getLatLng().latitude));
                    textLong.setText(String.valueOf(place.getLatLng().longitude));
                }
            }
        }
    }

    private void askBeforeExit() {
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setMessage(R.string.device_leave_confirm);
        b.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        b.setNegativeButton(R.string.no, null);
        b.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, R.string.device_save).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                Pattern p = Pattern.compile("^(([0-9A-F][0-9A-F][:]){5}[0-9A-F][0-9A-F])$");
                if (!p.matcher(textMAC.getText().toString()).matches()) {
                    Toast.makeText(this, R.string.device_mac_invalid, Toast.LENGTH_LONG).show();
                    return true;
                }
                Realm r = Realm.getDefaultInstance();
                r.beginTransaction();
                if (device == null) {
                    if (Device.getByMAC(textMAC.getText().toString()) != null) { //there is already a device with the same MAC saved
                        Toast.makeText(this, R.string.device_save_collision, Toast.LENGTH_LONG).show();
                    }
                    device = r.copyToRealm(new Device());
                }
                device.setName(textName.getText().toString());
                device.setMAC(textMAC.getText().toString());
                if (!textLat.getText().toString().isEmpty()){
                    device.setLatitude(Float.valueOf(textLat.getText().toString()));
                }
                if (!textLong.getText().toString().isEmpty()){
                    device.setLongitude(Float.valueOf(textLong.getText().toString()));
                }
                r.commitTransaction();
                finish();
                return true;
            case android.R.id.home:
                askBeforeExit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
