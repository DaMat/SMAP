package cz.damat.venisontracker.views;

import android.app.ActionBar;
import android.app.ProgressDialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import cz.damat.venisontracker.R;

/**
 * SuperClass Activity dealing with activity orientation
 */
public class SuperClassActivity extends Activity {
    protected boolean fullscreen = true;
    protected ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        int orientation = Integer.valueOf(sharedPreferences.getString(C.SP_KEY_ORIENTATION, String.valueOf(C.SP_DEF_ORIENTATION)));
//        switch (orientation) {
//            case C.SP_VAL_ORIENTATION_LANDSCAPE:
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//                break;
//            case C.SP_VAL_ORIENTATION_PORTRAIT:
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                break;
//            default:
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//                break;
//        }
//
//        if (!fullscreen) return;
//        getWindow().getDecorView().setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        if (!fullscreen) return;
//        if (hasFocus) {
//            getWindow().getDecorView().setSystemUiVisibility(
//                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                            | View.SYSTEM_UI_FLAG_FULLSCREEN
//                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
//        }
//    }

    /**
     * Same as {@link #showProgressDialog(String)}
     *
     * @param message
     */
    protected void showProgressDialog(int message) {
        showProgressDialog(getString(message));
    }

    /**
     * Creates and shows a new progress dialog (hiding an old one) with the provided message and forbidden cancellation
     *
     * @param message
     */
    protected void showProgressDialog(String message) {
        showProgressDialog(message, false);
    }

    /**
     * Same as {@link #showProgressDialog(String, boolean)}
     *
     * @param message
     * @param cancellable
     */
    protected void showProgressDialog(int message, boolean cancellable) {
        showProgressDialog(getString(message), cancellable);
    }

    /**
     * Creates and shows a new progress dialog (hiding an old one) with the provided message
     *
     * @param message
     * @param cancellable FALSE - if the dialog should NOT be dismissable by the user <br>
     *                    TRUE - if user should be able to cancel the dialog by button pressing
     */
    protected void showProgressDialog(String message, boolean cancellable) {
        if (progressDialog != null) {
            hideProgressDialog();
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        if (cancellable) {
            progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    onProgressDialogCancel();
                }
            });
        }
        progressDialog.show();
    }

    /**
     * Called when user click the cancel button (if allowed) of the progress dialog.
     * Children should override this method to do whatever is needed (e.g. cancel async task)
     * Does nothing in {@link SuperClassActivity}
     */
    protected void onProgressDialogCancel() {
    }

    /**
     * Same as {@link #setProgressDialogMessage(String)}
     *
     * @param message
     */
    protected void setProgressDialogMessage(int message) {
        setProgressDialogMessage(getString(message));
    }

    /**
     * Sets the message of the (not) null progress dialog
     *
     * @param message
     */
    protected void setProgressDialogMessage(String message) {
        if (progressDialog != null) {
            progressDialog.setMessage(message);
        }
    }

    /**
     * Dismisses and nulls the progress dialog
     */
    protected void hideProgressDialog() {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (Exception ignored) {
                //might happen if the activity is already closed
            }
        }
    }
}
