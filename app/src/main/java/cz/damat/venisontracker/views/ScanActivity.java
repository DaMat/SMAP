package cz.damat.venisontracker.views;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import cz.damat.venisontracker.helpers.BTConnection;
import cz.damat.venisontracker.R;
import cz.damat.venisontracker.model.Device;

public class ScanActivity extends SuperClassActivity {
    boolean adding = false;
    boolean reading = false;
    DeviceAdapter adapter;
    BluetoothAdapter btAdapter;
    ListView listView;

    BroadcastReceiver recDiscovery = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device != null) {
                    discovered(device);
                }
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                if (!reading) { //dont restart discovery when reading values
                    startDiscovery();
                } else {
                    discoveryEnd();
                }
            }
        }
    };
    private final BroadcastReceiver recState = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_ON) {
                    startDiscovery();
                } else {
                    //BT not on
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_scan);
        adding = getIntent().getExtras().getBoolean("ADDING", false);
        setTitle(adding ? R.string.title_activity_add : R.string.title_activity_connect);
        adapter = new DeviceAdapter(this);
        listView = (ListView) findViewById(R.id.listBTDevices);
        listView.setAdapter(adapter);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            Toast.makeText(this, R.string.scan_no_bt, Toast.LENGTH_SHORT).show();
            finish();
        }

        IntentFilter discoveryFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        discoveryFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(recDiscovery, discoveryFilter);
        registerReceiver(recState, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

        if (!btAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 666);
        } else {
            startDiscovery();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onListItemClick(adapter.getItem(i));
            }
        });
    }

    private void startDiscovery() {
        if (btAdapter.startDiscovery()) {
            setProgressBarIndeterminateVisibility(true);
        }
    }

    private void discoveryEnd() {
        setProgressBarIndeterminateVisibility(false);
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(recDiscovery);
        } catch (Exception ignored) {
        }
        try {
            unregisterReceiver(recState);
        } catch (Exception ignored) {
        }
        super.onDestroy();
    }

    private void onListItemClick(BluetoothDevice btDevice) {
        if (adding){
            Intent i = new Intent(this, DeviceActivity.class);
            i.putExtra("MAC", btDevice.getAddress());
            startActivity(i);
        } else {
            final Device device = Device.getByMAC(btDevice.getAddress());
            reading = true;
            showProgressDialog(R.string.progress_connecting);
            new BTConnection().readValues(btDevice, new BTConnection.ReadResultListener() {

                @Override
                public void onProgress(int progressType) {
                    int message = 0;
                    switch (progressType){
                        case PROGRESS_CONNECTING:
                            message = R.string.progress_connecting;
                            break;
                        case PROGRESS_SENDING:
                            message = R.string.progress_sending;
                            break;
                        case PROGRESS_RECEIVING:
                            message = R.string.progress_receiving;
                            break;
                        case PROGRESS_PARSING:
                            message = R.string.progress_parsing;
                            break;
                    }
                    final int finMessage = message;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setProgressDialogMessage(finMessage);
                        }
                    });
                }

                @Override
                public void onValuesReceived(final long[][] values) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setProgressDialogMessage(R.string.progress_saving);
                            device.addTimeStamps(values);
                            hideProgressDialog();
                            reading = false;
                            startDiscovery();
                            Toast.makeText(ScanActivity.this, R.string.scan_reading_done, Toast.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onError(Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideProgressDialog();
                            Toast.makeText(ScanActivity.this, R.string.scan_reading_error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
        }
    }

    private void discovered(BluetoothDevice btDevice) {
        if (adding || Device.getByMAC(btDevice.getAddress()) != null) { //if adding new device or the device is saved
            for (int i = 0; i < adapter.getCount(); i++) {
                if (adapter.getItem(i).getAddress().equals(btDevice.getAddress())) {
                    return;
                }
            }
            adapter.add(btDevice);
            adapter.notifyDataSetChanged();
        }
    }

    private class DeviceAdapter extends ArrayAdapter<BluetoothDevice> {
        static final int RESOURCE = android.R.layout.simple_list_item_1;

        public DeviceAdapter(Context context) {
            super(context, RESOURCE);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(getContext(), RESOURCE, null);
            }
            BluetoothDevice btDevice = getItem(position);
            Device device = Device.getByMAC(btDevice.getAddress());
            String text = device == null ? btDevice.getAddress() : device.toString();
            ((TextView) convertView.findViewById(android.R.id.text1)).setText(text);

            return convertView;
        }
    }
}
