package cz.damat.venisontracker.model;

import io.realm.RealmObject;

/**
 */
public class Timestamp extends RealmObject {
    long time;

    public Timestamp() {
    }

    public Timestamp(long time) {
        this.time = time;
    }

    /**
     * @return value of {@link #time}
     **/
    public long getTime() {
        return time;
    }

    /**
     * @param time {@link #time}
     * @return This Timestamp for setter chaining
     **/
    public Timestamp setTime(long time) {
        this.time = time;
        return this;
    }
}
