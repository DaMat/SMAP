package cz.damat.venisontracker.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 */
public class Device extends RealmObject {
    String name;
    String MAC;
    long startTime;
    float latitude;
    float longitude;
    RealmList<Timestamp> timestamps;

    /**
     * @return value of {@link #name}
     **/
    public String getName() {
        return name;
    }

    /**
     * @param name {@link #name}
     * @return This Device for setter chaining
     **/
    public Device setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * @return value of {@link #MAC}
     **/
    public String getMAC() {
        return MAC;
    }

    /**
     * @param MAC {@link #MAC}
     * @return This Device for setter chaining
     **/
    public Device setMAC(String MAC) {
        this.MAC = MAC;
        return this;
    }

    /**
     * @return value of {@link #startTime}
     **/
    public long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime {@link #startTime}
     * @return This Device for setter chaining
     **/
    public Device setStartTime(long startTime) {
        this.startTime = startTime;
        return this;
    }

    /**
     * @return value of {@link #latitude}
     **/
    public float getLatitude() {
        return latitude;
    }

    /**
     * @param latitude {@link #latitude}
     * @return This Device for setter chaining
     **/
    public Device setLatitude(float latitude) {
        this.latitude = latitude;
        return this;
    }

    /**
     * @return value of {@link #longitude}
     **/
    public float getLongitude() {
        return longitude;
    }

    /**
     * @param longitude {@link #longitude}
     * @return This Device for setter chaining
     **/
    public Device setLongitude(float longitude) {
        this.longitude = longitude;
        return this;
    }

    /**
     * @return value of {@link #timestamps}
     **/
    public List<Timestamp> getTimestamps() {
        return timestamps;
    }

    public long[] getTimestampsArray(){
        long tss[] = new long[this.timestamps.size()];
        for (int i = 0; i < tss.length; i++){
            tss[i] =  this.timestamps.get(i).getTime();
        }
        return tss;
    }
    /**
     * @param timestamps {@link #timestamps}
     * @return This Device for setter chaining
     **/
    public Device setTimestamps(RealmList<Timestamp> timestamps) {
        this.timestamps = timestamps;
        return this;
    }


    /**
     * @return All devices
     */
    public static RealmResults<Device> getAll(){
        return Realm.getDefaultInstance().where(Device.class).findAll();
    }

    /**
     * @param MAC
     * @return Saved device with the given MAC or null if no such device is present
     */
    public static Device getByMAC(String MAC){
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Device.class)
                .equalTo("MAC", MAC)
                .findAll().first(null);
    }

    public Device addTimeStamps(long[][] tsArrays){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        List<Timestamp> timestamps = new ArrayList<>();
        for (long[] tsArray : tsArrays){
            for (long timestamp : tsArray){
                timestamps.add(realm.copyToRealm(new Timestamp(timestamp)));
            }
        }
        this.timestamps.addAll(timestamps);
        realm.commitTransaction();
        return this;
    }

    @Override
    public String toString() {
        return getName() != null ? (getName() + " (" + getMAC() +")") :(getMAC());
    }
}
